Rebol [
	title: "Grab portrait data from TJ Website"
	date: 9-Jan-2020
	author: "Graham Chiu"
	name: %grab-tj-pdfs.reb
	notes: {
		use: r3 build-tj-portraits.reb

		builds the webpage to host these images

		<div class="col-sm-2 mt-sm-1">
            <div class="card portrait-card">
                
                <img src="/assets/portrait/F174184__FillMaxWzEwMCwxMjVd.jpg" class="card-img-top" alt="YIU Che Lam">
                
                <div class="card-body">
                    <h6 class="card-title">YIU Che Lam</h6>
                    <h6 class="card-title"></h6>
                    <p class="card-text">174184</p>
                    <a href="/portraits/show/1708" class="btn btn-primary">Read More</a>
                </div>
            </div>
        </div>
	}
]

src: https://www.tungjung.org.nz/portraits?start=
base: https://www.tungjung.org.nz

lc: 0
page: unspaced ["| Portraits ||||||" newline "|:------:|:------:|:------:|:------:|:------:|:------:|" newline]
linect: 0

name-row: copy ""
id-row: copy ""

for counter 0 168 24 [ ; go through each page
	data: to-text read join src counter
	parse data [some [
		to {<img src="/assets/portrait/} 
			thru {<img src="} copy link to {"} 
			thru {<h6 class="card-title">} copy name to </h6> </h6> 
			thru <p class="card-text"> copy referenceid to </p> (
			if %.jpg = suffix? link [
				;attempt [
					imagename: to-file last split-path link
					dump imagename
					dump name
					dump referenceid
					; now construct the table cell
					if 6 = linect: me + 1 [
						; start a new row
						append page join "|" newline
						append page unspaced [ name-row "|" newline ]
						; append page unspaced [ id-row "|" newline ]
						name-row: copy ""
						id-row: copy ""
						linect: 0
					]
					; <img src="./img.png" width="100" height="100">
					append page unspaced [{|<img src="assets/} imagename {" alt="} name {" height="100px" width="170px">}]
					append name-row unspaced ["| " name " `id=" referenceid "`"]
					; append id-row unspaced ["| " referenceid]
					; append page unspaced ["|![ " name " ](assets/" imagename ")"]
				;]
			]
		) | 
		( lc: me + 1 print spaced ["not a portrait link" lc] )
		to {<a href="} thru ">"
	]]
]

append/dup page "|" linect 
append page newline
append page unspaced [ name-row "|" newline ]
; append page unspaced [ id-row "|" newline ]


write %..\docs\portraits\test.md page

print "Finished"