Rebol [
	title: "Grab portrait files from TJ Website"
	date: 9-Jan-2020
	author: "Graham Chiu"
	name: %grab-tj-graves.reb
	notes: {
		use: r3 grab-tj-graves.reb

		Searches the Wellington Cemetery records looking for each name in the portraits

		sample data to parse:

		<div class="col-sm-2 mt-sm-1">
            <div class="card portrait-card">
                
                <img src="/assets/portrait/F174184__FillMaxWzEwMCwxMjVd.jpg" class="card-img-top" alt="YIU Che Lam">
                
                <div class="card-body">
                    <h6 class="card-title">YIU Che Lam</h6>
                    <h6 class="card-title"></h6>
                    <p class="card-text">174184</p>
                    <a href="/portraits/show/1708" class="btn btn-primary">Read More</a>
                </div>
            </div>
        </div>
	}
]

search-string: [ {https://wellington.govt.nz/services/community-and-culture/cemeteries/search-cemetery-records/results?serviceType=All&firstNames=} firstname "&lastName=" surname "&fromDate=01%2f01%2f1840&toDate=10%2f01%2f2020&cemetery=All" ]

src: https://www.tungjung.org.nz/portraits?start=
base: https://www.tungjung.org.nz
plinth: https://wellington.govt.nz

lc: 0
digit: charset [ #"0" - #"9"]
digits: [some digit]

possibles: copy []

for counter 0 168 24 [ ; go through each page
	data: to-text read join src counter
	parse data [some [
		thru {<div class="card portrait-card">} thru {<h6 class="card-title">} copy name to </h6> thru </h6> (
			; we got the name from the portrait card
			dump name
			set [surname firstname middlename] split name space
			; now try to read a cemetery record
			; print unspaced search-string
			cemetery: to text! read to-url unspaced search-string
			if parse cemetery [ thru "Returned: " copy num digits any space "Results" to end][
				num: to integer! num
				if num = 0 [
					; print spaced ["No record found for" name]
				] else [
					; we found a possible match on the WCC fuzzy search
					parse cemetery [ thru "Returned: " thru <table> thru {<a href="} copy link to {">} thru {">} copy cand-surname to </a> thru <td> copy candidate to </td> to end]
					if not find/case candidate "BABY" [ ; remove any babies
							print spaced ["**** Record found for" name "as" candidate]
							; now read the first cemetery link to see what sort of service was provided
							record: to text! read join plinth link

		                    parse record [thru {<th>Service provided</th>} thru <td> copy service to </td> to end]

							append/only possibles reduce [name unspaced ["**" cand-surname "** " candidate] join plinth link service]
					]
				]
			] else [
				print "Failed to parse a result from cemetery search"
			]
		) | 
		( lc: me + 1 print spaced ["not a portrait link" lc] )
		to {<a href="} thru ">"
	]]
]

; got all the candidates, now produce a markdown page

data: unspaced ["# Possible Candidate ID for Portraits" 
	newline newline 
	"| Name | Candidate | Link | Type |" newline "|:------|:------|:------|:-----|" 
	newline
]

for-each person possibles [
	append data unspaced [
		"| " person/1 " | " person/2 " | [link](" person/3") | " person/4 " |"  
		newline
	]
]

write %..\docs\portraits\possibles.md data

print "Finished, and file is saved as possibles.md"
halt