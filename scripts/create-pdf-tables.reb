Rebol [
	title: "Create download table"
	file: %create-pdf-tables.reb
	date: 9-Jan-2020
	author: "Graham Chiu"
	notes: {
		reads the current directory of PDFs and builds the table for downloads

		eg:

		## 2019

		| Season			| Download Link |
		|:------------------|:--------------|
		| Summer			| [7.65Mb](assets/Summer-issue-2019-v2.pdf) 	|
		| Spring			| [4.13Mb](assets/Spring-2019-issue.pdf) 	|
		| Winter			| [5.30Mb](assets/Winter_2019_issue.pdf) 	|
		| Autumn			| [5.20Mb](assets/Autumn_2019_issue.pdf)  |
	}
]

files: read %./

data: copy ""

for year 2019 2009 -1 [
	; create the header for the current table first
	period: unspaced ["## " year newline newline "| Season			| Download Link |" newline "|:------------------|:--------------|" newline]
	for-each file files [
		for-each season ["Summer" "Spring" "Winter" "Autumn"][
			if all [
				find file season
				find file form year
			][
				filesize: any [attempt [round/to divide get in info? file 'size 1000000 .01] 0]
				append period unspaced [
					"|" season "| [" 
					filesize "Mb](" 
					"assets/" file ") |" 
					newline
				]
			]
		]
	]
	append data period
	append data newline
]

print data
write clipboard:// data

; control-V to paste the tables into your text processor
