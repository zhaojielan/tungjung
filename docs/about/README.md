# Welcome to the Tung Jung Association of New Zealand

The Association was founded in 1926 by the Chinese who immigrated to New Zealand from the Tung Gwoon (Dongguan) and Jungsen (Zengcheng) hence Tung Jung, districts in the Guangdong province in Southern China.

This web site gives an overview of the history and the people who founded the Association and an insight into the work of the Association at the present time.

We hope that this web site will stimulate interest in the Association and in the history of the Chinese in New Zealand.

## Full Membership

All people whose ancestry are from the counties of Tung Gwoon (Donguan) and Jungsen (Zengcheng) in Guangdong province in China are invited to apply for full membership to the Association.

## Associate Membership

People who do not qualify for full membership but wish to partake in the activities of the Tung Jung Association are welcome to apply for associate membership to the Association.  However, the constitution does not allow voting rights for associate members.  

Associate members may have ancestral homes outside of the Tung Jung area, and do not need to have any Chinese ancestry.  

:orange_book: [Membership form 2024](/contact/membership-2025.pdf) 

:orange_book: [Google Form](https://forms.gle/KNSD4QoxF5uWCj3K8) to fill in web based membership form

:orange_book: [Constitution Revised May 2001](assets/TungJung_Constitution.pdf)

## Charitable Status

See [Charities register](https://register.charities.govt.nz/Charity/CC34048) for summary

## Wellington Chinese Communities Groups Trustee Ltd (WCCG Trustee Ltd)

The WCCG Trustee Ltd is a Corporate Trustee company, that provides a legal framework for the Wellington Chinese Community Groups Trust, the latter being a registered charity - [CC48433](https://register.charities.govt.nz/CharitiesRegister/ViewCharity?accountId=4c061889-5042-e111-aa46-00155d741101).  The Tung Jung Association of NZ Inc is a founding member of this umbrella company, WCCG Trustee Ltd.  See [NZ Companies Office for details](https://app.companiesoffice.govt.nz/companies/app/ui/pages/companies/3690561).  WCCG Trustee Ltd was incorporated in December 2011 to help raise funds for the devastating Christchurch Earthquake of [February 2011](https://nzhistory.govt.nz/page/christchurch-earthquake-kills-185) via the Wellington Chinese Community Groups Trust. Subsquently these organisations were kept to organise local Chinese cultural events   See [more](/wccg/about.md)

 