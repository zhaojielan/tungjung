# 2024 MEETINGS AND EVENTS

New Year's Dinner Dragon's Restaurant Sunday 18 Feb, 2024 6 pm $50 pp

- AGM 18th August 2024 2pm at the [Johnsonville Collective Community Hub](https://www.collectivehub.co.nz).  This will be folowed by a hot pot dinner for those attending the AGM at the [Babaili Restaurant](https://www.babaili.co.nz/babaili-hot-pot)

## Executive Committee Meetings

Executive Committee meetings usually start at 7:30 pm on the first Monday of each month, commencing 6th Feb 2023

# Social

- Ching Ming Sunday 7th April, 2024 12 pm [Tung Jung Memorial Stele](/genealogy/karori-cemetery.md)

- Mid-Winter Yum Cha 5th June, 2024 12 pm at Dragon's Restaurant.  $15 pp for members, $25 pp non-members. 

- Moon Festival (中秋節) 22nd September, 2024.  6 pm Dragon's Restaurant. $45 pp for members, and $50 for non-members. Please book tables with [Peter Moon](https://tungjung.nz/contact/#social-committee)

- [Chung Yeung](/genealogy/karori-cemetery.md) Sunday 13th October, 2024 (9th day of the 9th month of the Lunar Calendar)

- Christmas Seniors' Yum Cha Wednesday 11th December, 2024

