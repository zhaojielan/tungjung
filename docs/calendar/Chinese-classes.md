# Chinese Language Classes (online/physical)

Cantonese Class -online restarts August 2024, last class 10 Dec 2024.  First class of 2025 is 14 January 2025  
Mandarin Class restarts 12th January, 2024 

## Cantonese Classes

### Virtual

Please contact the Secretary, [Helen South](/contact/#english-secretary), if you wish to join the online Cantonese classes for beginners

Virtual classes are held using Google Meet at  <https://meet.google.com/zjp-aicd-gcv> with 
* Beginnger's class 6:30 pm Tuesdays
* Intermediate class 7:30 pm Tuesdays
* Advanced class (starting 2025 Academic Year - enrol with secretary@tungjung.nz)
* Class materials are on a [shared google drive](https://drive.google.com/drive/u/1/folders/1l3H2xhThZ1Qg0ryJ8he1G1-rVHAwukJ8)
* Online basis for the class follows [Mangolabs online Cantonese](https://learn.mangolanguages.com/pathway/from/en-US/to/yue-Hant/722/1/1/1) - Wellington City Library users have free access to this resource.  Those living outside Wellington can ask a family member in Wellington to add them to their family group to access this service.
* Introduction to the 6 tones of Cantonese with [Brittany Chan](https://www.youtube.com/watch?v=b38H_ySiTd4)
* Tone practice with [Mrs Ng Cantonese](https://youtu.be/BXUbcoMfFbs?si=s1yuPx4bZaHbQv9k)

### Physical

Nil at present

## Mandarin Classes

Beginner's class is online on Fridays at 7:30 pm.  Contact [Graham Chiu](/contact/#vice-president) for the details.  Classes are limited to 3 students.

## Language Learning

Attending classes helps you learn some grammar, tones, and vocabulary.  However, the bulk of your learning needs to be done via a mechanism called language acquisition via the consumption of comprehensible input.  Most of this needs to be pitched at a level just slightly higher than your current level so you keep increasing your acquisition.

How to learn a new language with [Dr Jeff McQuillan](https://www.youtube.com/watch?v=9Olt2FO99SQ)

## Poll Tax Fund Support

As of November 2024 this class is now being partially funded by a grant (R-CPTHT-2025-283854) from the Chinese Poll Tax Heritage Fund.

![](assets/CPTT-Logo-August-2015.png)
