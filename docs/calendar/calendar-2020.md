# 2020 MEETINGS AND EVENTS

 

## Executive Committee Meetings and AGM

Executive Committee meetings usually start at 7:30 pm

- February 3                                  

- March 2     

** Meetings all moved to online until further notice ** 



- April 6                                             

- May 4                         

- June 1 (Queen's Birthday Weekend)                        

- July 6

- August 3

- **August 16 AGM (Sunday)**

- September 7  

- October 5

- November 2 

- November TBA (Zoom meeting with NZCA President Richard Leung regarding media and the Chinese Community)

- December 7    

 

## Social Calendar                                               

- Chinese New Year. Grand Century Restaurant Sunday 26 January 6:30 pm
    
    Contact <a href="/contact/#president">Peter Moon</a> to book a table or seat.  Tickets are $40 per head. 

    Menu is [here](assets/Chinese-New-Years-Menu-2020.pdf)

    :star: :star: :star: Closing date for tickets 17th January, 2020 :star: :star: :star:

- Clean Up and Inventory           February Weekend TBA

** Meetings all moved to online or postponed until further notice ** 


- ~~Qing Ming Festival                     Sunday 5 April~~ Held online

- ~~Mid-Year Senior Yum Cha       Wednesday 17 June~~ cancelled

- ~~Moon Festival                              Sunday 4 October~~ cancelled 

- Photo session 18th October, 2021 13:30 - 17:00  
  Linking Chinese New Zealanders to their past, a online photographic project by [ObjectAffection](https://www.objectaffection.com/) 

- Chung Yeung Festival ([Karori Cemetery Tung Jung Monument](/genealogy/karori-cemetery.md)) Sunday 25 October (This is still happpening ... please let [Peter Moon](/contact/#president) know if you are attending so we can plan the catering)

- Christmas Senior Yum Cha     Wednesday 2 December 12 pm at Dragon's Restaurant (note change of date from 9th December)  
  Contact [Virginia Ng](/contact/#treasurer) for tickets.  
  And here's a collection of [photos](https://photos.app.goo.gl/WB9AsDd3HXK7xNRB6) from the event taken by Ken Chung

- Seniors Visit                 Saturday in December 19th, 2020

