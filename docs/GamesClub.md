# Wellington Mahjong & Chinese Games Club

**Club Details**

- Monthly Saturday afternoon, 1 - 5.30pm (drop in anytime)
- Wellington Bridge Club, 17 Tinakori Rd, Thorndon 
- $10 per person
- Tea and coffee provided. Please bring light afternoon tea to share.

**Sign up**: https://forms.gle/6oLoGpm9tddREFdT7


**Calling all Mahjong, Chinese Chess (Xiangqi) and Go (Weiqi) players**

Whether you're a complete beginner or have some skills under your belt, come along to enjoy some friendly games at our Saturday afternoon club sessions. There is no gambling involved.

Drop in anytime, for a few hours or more. Bring along friends and family

**Game sets**: Please bring along your Mahjong, Chinese Chess and Go sets.

**Payment options**: 
Cash, Internet banking, or Credit card with surcharge.

_Internet banking: 
- Chinese Games Club 02-0500-0677990-003
- Particulars: Name 
- Reference: Event date or month(s)_


Contact: Lucinda treasurer@tungjung.nz for more information.

**Upcoming Sessions**:

* 17 Aug
* 31 Aug (with Wellington China Tongue)
* 14 Sept
* 19 Oct
* 23 Nov


**Venue info**:

- Accessibility: There is outdoor footpath access into the building.
- Parking: Limited Off-street parking is available beneath the building.


_Sponsored by:
Tung Jung Association of New Zealand (新 西 蘭 東 增 會 館)_
