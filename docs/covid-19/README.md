# COVID-19 Global Pandemic 

## Alert Level 4 commenced (midnight of Wednesday 25th March)

[What are the levels?](https://static3.stuff.co.nz/new-zealand-covid-19-alert-level-49b3bcc9.pdf)

## Let us help you, and you help others in the Chinese Community

!!!! [Questionnaire on your needs](https://forms.gle/FxHh7tBFXDq7nooW7) !!!!

!!! [关于您需求的调查问卷](https://docs.google.com/forms/d/e/1FAIpQLSfFA5SKc150PSXBt3PggUes95BvxZY5_4aXQ2xdlgoQdxE1vg/viewform?usp=sf_link) !!!

## What does isolation mean?

[NZ Office of Ethnic Communities (Cantonese) Youtube](https://www.youtube.com/watch?v=Tzm28693BMc&feature=youtu.be)

[潮州話](https://youtu.be/pZTOFpQTZXw)

[齐心协力，延缓病毒传播](https://covid19.govt.nz/assets/translations/COVID_what-you-need-to-know_Chinese.pdf)

[Videos from Edmonton, Canada in Cantonese](https://www.cbc.ca/news/canada/edmonton/covid-19-chinese-seniors-edmonton-chinatown-care-centre-1.5505156)

## Social Distancing

[New South Wales Govt Chinese Pamphlet](social-distancing-messages-chinese-traditional.pdf)


## Contact Numbers

### New Zealand Wide (local regions to follow)

* English and Mandarin: Dr Graham Chiu **Mob:** 020 4103 0559 **Email:** graham.chiu@tungjung.nz
* Cantonese: Gordon Wu **Mob:** 027 487 5314 **Email:** gordon.wu@tungjung.nz

### Auckland Region (more to follow)

* English, Cantonese & Mandarin: John Ling **Mob:** 021 867 238 [Facebook - John Ling](https://www.facebook.com/ling.john.9400) - [Auckland Chinese Community Centre](https://www.aucklandchinese.org.nz/)
* David Tai **Mob:** 027 8666232 **Email:** d_tai@xtra.co.nz [Auckland Chinese Community Centre](https://www.aucklandchinese.org.nz/)

### Chinese Nationals

The Embassy of the People's Republic of China should be your first contact as they have a responsibility of care for their citizens

* Wellington 04-499502? (04-499 0419)
* Auckland 09-5251200
* Christchurch 021-1767288

### Healthline Specifically if you think you have COVID-19

* 0800 358 5453 
* +64 9 358 5453 (international sim card)

### Financial and Food Assistance

* 0800 222 296 between 7.00 a.m. – 7.00 p.m. to ask for help with food
* 0800 779 997 (8am–1am, 7 days a week) for financial assistance

## Covid-19 NZ Map of Infections

See [Map](https://covid19map.nz)


## Symptoms

![](assets/NED-1370-COVID19-Identifying-the-symptoms_eeboNAzC.jpg)


## Map

This is a very frequently updated [map](https://gisanddata.maps.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6) of the virus spread throughout the world courtesy of John Hopkins Hospital

## Masks

Although some international experts __not__ living in virus infected areas are claiming that masks make no difference, or could increase the risk of disease (being uncomfortable, fingers more frequently touching the face to adjust the mask) the official Chinese government position is that masks be worn. Standard surgical masks are worn to protect others by containing the wearer's cough droplets, but because they're not tight fitting, they do little to protect the wearer from getting infected.  Many will wear double surgical masks.  The recommended types are N95, or the European equivalent, P2.  The P2V seems to be a P2 with a valve on it.

## Call for Medical Equipment and Funds From Zengcheng

The Zengcheng Overseas Chinese Affairs Office had requested medical supplies and financial aid.  

The Tung Jung Association sent NZ$2,270 including donations from members and this was gratefully received.

## Covid-19 Statement to the Community from the Minister of Health, NZ 

Date: 30 January 2020

See David Clark, Minister of Health [statement](assets/Letter_to_Chinese_community_from_Greg_OConnor_20200130.pdf)
