# New Year of the Rat (2020)

We had a great time at the [Grand Century Restaurant](https://www.facebook.com/pages/Grand-Century/191882514169818) in Te Aro on the 26th January 2020. Almost 26 tables of 10 people came to the celebrations and to enjoy a feast.

Prizes were given to all those born in the year of the Rat, and table prizes were also given to lucky patrons.  The raffle attracted a lot of interest with many winners of a fine selection of goods and services.

## Videos

The [Chinese Anglican Mission](http://wn.anglican.org.nz/parishes___churches/wellington_north/chinese_anglican_mission) put on a well enjoyed Lion Dance.

<iframe class="embed-responsive-item" src="https://youtube.com/embed/7ZAAFk4e0dI" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Grace Yuan Yuan Chen sang two songs for us.  She previously had sung for the President of The People's Republic of China.  _My Heart Will Go On_ by Celine Dion at 00:40, and _Tian Mi Mi_ by Teresa Teng (鄧麗君) at 6:07


<iframe class="embed-responsive-item" src="https://youtube.com/embed/V2BbeJIyYrw" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
