# Current Events

## Wellington Chinese Games Club

* Come play Mahjong (麻將), Chinese Chess (象棋) and Go (圍棋)
* Regular sessions - see [web pages](/games/)
* Previous sessions (to be done)

## Wellington Chinese Community Groups

* Newtown Community Centre   
  Address: Colombo Street &, Rintoul Street, Newtown, Wellington 6021
, Sunday 11 August 2024 15:00 - 16:00
* Lantern Festival 2024 review
* National Day Dinner planning
* Lantern Festival 2025 planning

尊敬的各位侨领，
温馨提醒：
8月11日周日下午三点-四点的惠灵顿侨社代表会议地址改为Newtown社区中心。会议议程包括2024年惠灵顿元宵灯节总结，及2024年国庆晚餐会和2025年惠灵顿元宵灯节的筹备计划等。地址：Colombo Street &, Rintoul Street, Newtown, Wellington 6021。如不能出席请告知。

Rachel Qi，齐慧芳 <qiqi957@hotmail.com>

## National Day Dinner for 2024

* August 29, at 6:00 pm Grand Century Restaurant
* Tickets $56/seat. Purchase seats via your local organisation (TungJung via <secretary@tunjung.nz>)
