# Contact Details

## Tung Jung Association

```
Tung Jung Association of NZ Inc
33 Torrens Terrace
PO Box 9058
Wellington 6041
NEW ZEALAND
email: secretary@tungjung.nz
```

## Membership

The financial year for the Association begins on 1 April. To ensure your
membership remains current please print off the membership form and send it
with your payment to PO Box 9058 Wellington. Please include your ancestral
village.  Or, fill in the online Google form. This will add to the records of the Association.

Senior members over the age of 70 years do not need to pay but should return
the membership form so that membership records can be kept up to date.  This is a legal 
requirement of the Charities Commission to allow us to function as a charity.

Click [here](/contact/membership-2025.pdf) for the pdf version of the Membership Form April 2024 - March 2025 

Or, use the [Google Form](https://forms.gle/KNSD4QoxF5uWCj3K8) to do this electronically

## President

```
Dr Graham Chiu 趙世榮
Karori
Wellington
Mobile +64 020 41034348
Email: graham.chiu@tungjung.nz president@tungjung.nz
NEW ZEALAND
```

## English Secretary

```
Helen South
Mobile 
Email: secretary@tungjung.nz
```

## Chinese Secretary

```
Kevin Zeng 曾凯文
Newtown
Wellington
Kevinzeng63@gmail.com  
```

## Webmaster

```
Dr Graham Chiu 趙世榮
Email: graham.chiu@tungjung.nz
```

## Vice President

```
Rayward Chung  鍾振威
Email: Ray.Chung@wcc.govt.nz
```

## Treasurer

```
Lucinda Chiu 趙潔蘭
Email: treasurer@tungjung.nz
```

## Social Committee

``` 
Peter Moon 歐偉權 04 3898819 peteraumoon@yahoo.co.nz (convenor)
Valerie Ting 陳惠娌 027 4955331 valerie.ting8@gmail.com
```
